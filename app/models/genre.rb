class Genre < ActiveRecord::Base
  has_and_belongs_to_many :musicians
  validates :title, presence: true
end
