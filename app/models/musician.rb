class Musician < ActiveRecord::Base
  has_and_belongs_to_many :genres
  validates :name, presence: true
  scope :musicians_and_genres, -> {
    includes(:genres).map { |musician| musician.as_json.merge({genres: musician.genres.as_json}) }
  }
end
