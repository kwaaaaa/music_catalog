@Genre = React.createClass
  genreRow: ->
    React.DOM.tr null,
      React.DOM.td null, @props.genre.title
      React.DOM.td null,
        React.DOM.a
          className: 'btn btn-default'
          onClick: @handleToggle
          'Edit'
        React.DOM.a
          className: 'btn btn-danger'
          onClick: @handleDelete
          'Delete'
  genreForm: ->
    React.DOM.tr null,
      React.DOM.td null,
        React.DOM.input
          className: 'form-control'
          type: 'text'
          defaultValue: @props.genre.title
          ref: 'title'
      React.DOM.td null,
        React.DOM.a
          className: 'btn btn-default'
          onClick: @handleEdit
          'Update'
        React.DOM.a
          className: 'btn btn-danger'
          onClick: @handleToggle
          'Cancel'
  getInitialState: ->
    edit: false
  handleToggle: (e) ->
    e.preventDefault()
    @setState edit: !@state.edit
  handleEdit: (e) ->
    e.preventDefault()
    data =
      title: ReactDOM.findDOMNode(@refs.title).value
    $.ajax
      method: 'PUT'
      url: "/api/v1/genres/#{ @props.genre.id }"
      dataType: 'JSON'
      data:
        genre: data
      success: (data) =>
        @setState edit: false
        @props.handleEditGenre @props.genre, data
  handleDelete: (e) ->
    e.preventDefault()
    $.ajax
      method: 'DELETE'
      url: "/api/v1/genres/#{ @props.genre.id }"
      dataType: 'JSON'
      success: () =>
        @props.handleDeleteGenre @props.genre
  render: ->
    if @state.edit
      @genreForm()
    else
      @genreRow()