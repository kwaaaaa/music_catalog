@Musician = React.createClass
  musicianRow: ->
    React.DOM.tr null,
      React.DOM.td null, @props.musician.name
      React.DOM.td null,
        @returnUpdateTitle(@props)
      React.DOM.td null,
        React.DOM.a
          className: 'btn btn-default'
          onClick: @handleToggle
          'Edit'
        React.DOM.a
          className: 'btn btn-danger'
          onClick: @handleDelete
          'Delete'
  musicianForm: ->
    React.DOM.tr null,
      React.DOM.td null,
        React.DOM.input
          className: 'form-control'
          type: 'text'
          defaultValue: @props.musician.name
          ref: 'name'
      React.DOM.td null,
        for genre in @props.genres
          React.DOM.span
            key: genre.id
            className: 'radio-inline'
            genre.title + ' '
            React.DOM.input
              name: 'genre_ids'
              type: 'checkbox'
              value: genre.id
              key: genre.id
              defaultChecked: @props.musician.genres.map((g)-> g.title).indexOf(genre.title) > -1
              onChange: @handleChangeCheckBox
      React.DOM.td null,
        React.DOM.a
          className: 'btn btn-default'
          onClick: @handleEdit
          'Update'
        React.DOM.a
          className: 'btn btn-danger'
          onClick: @handleToggle
          'Cancel'
  getInitialState: ->
    edit: false
  handleToggle: (e) ->
    e.preventDefault()
    @setState edit: !@state.edit
    @setState genre_ids: @props.musician.genres.map((genre) ->
      genre.id)
  handleChangeCheckBox: (e) ->
    genre_ids = @state.genre_ids
    if e.target.checked == true
      genre_ids = React.addons.update(@state.genre_ids, { $push: [e.target.value] })
    if e.target.checked == false
      index = @state.genre_ids.indexOf e.target.value
      genre_ids = React.addons.update(@state.genre_ids, { $splice: [[index, 1]] })
    console.log('genre_ids after: ' + genre_ids)
    @setState genre_ids: genre_ids
  handleEdit: (e) ->
    e.preventDefault()
    data =
      name: ReactDOM.findDOMNode(@refs.name).value
      genre_ids: @state.genre_ids
    $.ajax
      method: 'PUT'
      url: "/api/v1/musicians/#{ @props.musician.id }"
      dataType: 'JSON'
      data:
        musician: data
      success: (data) =>
        @setState edit: false
        @props.handleEditMusician @props.musician, data
  handleDelete: (e) ->
    e.preventDefault()
    $.ajax
      method: 'DELETE'
      url: "/api/v1/musicians/#{ @props.musician.id }"
      dataType: 'JSON'
      success: () =>
        @props.handleDeleteMusician @props.musician
  render: ->
    if @state.edit
      @musicianForm()
    else
      @musicianRow()
  returnUpdateTitle: (props) ->
      for genre in props.genres
        if props.musician.genres.map((g)-> g.id).indexOf(genre.id) > -1
          genre.title + ' '


