var About = React.createClass({
  render() {
    return (
      <div>
        <h1>About</h1>
        Web-приложение "Cправочник музыкальных исполнителей".
        <br></br>
        Две визуальные таблицы: музыкантов и жанров. CRUD. Бэкэнд на RoR, фронтэнд на Bootstrap и React.
        <br></br>
        <a href="https://github.com/kwaaaaa/music_catalog">Github</a>
        <br></br>
        <a href="https://music-catalog.herokuapp.com/">Heroku</a>
      </div>
    )
  }
});

class ProductTable extends React.Component {
  render() {
    var rows = [];
    var lastCategory = null;
    this.props.products.forEach(function(product) {
      if (product.category !== lastCategory) {
        rows.push(<ProductCategoryRow category={product.category} key={product.category} />);
      }
      rows.push(<ProductRow product={product} key={product.name} />);
      lastCategory = product.category;
    });
    return (
    <div>
      <table>
        <thead>
        <tr>
          <th>Name</th>
          <th>Price</th>
        </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
      <button>
        Add to LocalStorage
      </button>
    </div>
    );
  }
}

class SearchBar extends React.Component {
  render() {
    return (
      <form>
        <input type="text" placeholder="Search..."/>
        <p>
          <input type="checkbox"/>
          {' '}
          Only show products in stock
        </p>
      </form>
    );
  }
}

class ProductCategoryRow extends React.Component {
  render() {
    return (
      <div>
      </div>
    );
  }
};

class ProductRow extends React.Component {
  render() {
    return (
      <div>
      </div>
    );
  }
};

class FilterableProductTable extends React.Component {
  render() {
    return (
      <div>
        <SearchBar />
        <ProductTable products={this.props.products} />
      </div>
    );
  }
}
