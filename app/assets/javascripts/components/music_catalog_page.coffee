@MusicCatalogPage = React.createClass
  getInitialState: ->
    genres: @props.genres
    musicians: @props.musicians
  render: ->
    React.DOM.div
      className: 'music-catalog-page'
      React.createElement Genres, genres: @state.genres, addGenre: @addGenre, deleteGenre: @deleteGenre, updateGenre: @updateGenre
      React.createElement Musicians, musicians: @state.musicians, genres: @state.genres
  addGenre: (genre) ->
    genres = React.addons.update(@state.genres, { $push: [genre] })
    @setState genres: genres
  deleteGenre: (genre) ->
    index = @state.genres.indexOf genre
    genres = React.addons.update(@state.genres, { $splice: [[index, 1]] })
    @replaceState genres: genres
  updateGenre: (genre, data) ->
    index = @state.genres.indexOf genre
    genres = React.addons.update(@state.genres, { $splice: [[index, 1, data]] })
    @replaceState genres: genres