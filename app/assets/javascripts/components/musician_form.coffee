@MusicianForm = React.createClass
  getInitialState: ->
    name: ''
    genre_ids: []
  render: ->
    React.DOM.form
      className: 'form-inline'
      onSubmit: @handleSubmit
      React.DOM.div
        className: 'form-group'
        React.DOM.input
          type: 'text'
          className: 'form-control'
          placeholder: 'Name'
          name: 'name'
          value: @state.name
          onChange: @handleChange
      React.DOM.div
        className: 'form-group'
        for genre in @props.genres
          React.DOM.span
            className: 'radio-inline'
            key: genre.id
            genre.title + ' '
            React.DOM.input
              name: 'genre_ids'
              type: 'checkbox'
              value: genre.id
              defaultChecked: false
              onChange: @handleChangeCheckBox
      React.DOM.button
        type: 'submit'
        className: 'btn btn-primary'
        disabled: !@valid()
        'Create musician'
  handleChange: (e) ->
    name = e.target.name
    @setState "#{ name }": e.target.value
  handleChangeCheckBox: (e) ->
    genre_ids = @state.genre_ids
    if e.target.checked == true
      genre_ids = React.addons.update(@state.genre_ids, { $push: [e.target.value] })
    if e.target.checked == false
      index = @state.genre_ids.indexOf e.target.value
      genre_ids = React.addons.update(@state.genre_ids, { $splice: [[index, 1]] })
    @setState genre_ids: genre_ids
  handleSubmit: (e) ->
    e.preventDefault()
    $.post '/api/v1/musicians/', { musician: @state }, (data) =>
      @props.handleNewMusician data
      @setState @getInitialState()
    , 'JSON'
  valid: ->
    @state.name