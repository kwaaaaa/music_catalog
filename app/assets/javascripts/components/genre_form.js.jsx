var GenreForm = React.createClass({
  getInitialState: function() {
    return {
      title: ''
    };
  },
  render: function() {
    return React.DOM.form({
      className: 'form-inline',
      onSubmit: this.handleSubmit
    }, React.DOM.div({
      className: 'form-group'
    }, React.DOM.input({
      type: 'text',
      className: 'form-control',
      placeholder: 'Title',
      name: 'title',
      value: this.state.title,
      onChange: this.handleChange
    })), React.DOM.button({
      type: 'submit',
      className: 'btn btn-primary',
      disabled: !this.valid()
    }, 'Create genre'));
  },
  handleChange: function(e) {
    var name, obj;
    name = e.target.name;
    return this.setState((
      obj = {},
        obj["" + name] = e.target.value,
        obj
    ));
  },
  postGenre: function(data) {
    $.ajax({
      data: data,
      url: '/api/v1/genres/',
      type: "POST",
      dataType: "json",
      success: function ( data ) {
        $(".sending-mess").hide();
        this.setState(this.getInitialState());
        this.props.handleNewGenre(data);
      }.bind(this),
      error: function() {
        var _this = this;
        $(".sending-mess").show();
        this.setState(this.getInitialState());
        setTimeout(function(){
          data._id = data._id  || Math.floor((Math.random() * 1000000) + 1);
          _this.saveInLocalStorage('notDeliveredGenres', data);
          _this.postGenre(data);
        }, 5000);
      }.bind(this)
    });
  },
  handleSubmit: function(e) {
    e.preventDefault();
    this.postGenre({genre: this.state});
  },
  valid: function() {
    return this.state.title;
  },
  saveInLocalStorage: function(key, data) {
    // localStorage.clear();
    var item = localStorage.getItem(key);
    var storeData = storeData || '';
    storeData += JSON.stringify(data);
    if (item !== null) {
      if (item.search('"_id":' + data._id.toString()) < 0) {
        localStorage.setItem(key, (item !== null ? item + storeData : storeData) + "|");
      }
    }
    else {
      localStorage.setItem(key, storeData + "|");
    }
    console.log(localStorage.getItem(key));
  }
});
