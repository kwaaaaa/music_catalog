@Musicians = React.createClass
  getInitialState: ->
    musicians: @props.musicians
  getDefaultProps: ->
    musicians: []
  addMusician: (musician) ->
    musicians = React.addons.update(@state.musicians, { $push: [musician] })
    @setState musicians: musicians
  deleteMusician: (musician) ->
    index = @state.musicians.indexOf musician
    musicians = React.addons.update(@state.musicians, { $splice: [[index, 1]] })
    @replaceState musicians: musicians
  updateMusician: (musician, data) ->
      index = @state.musicians.indexOf musician
      musicians = React.addons.update(@state.musicians, { $splice: [[index, 1, data]] })
      @replaceState musicians: musicians
  render: ->
    React.DOM.div
      className: 'musicians'
      React.DOM.h2
        className: 'title'
        'Musicians'
      React.createElement MusicianForm, handleNewMusician: @addMusician, genres: @props.genres
      React.DOM.hr null
      React.DOM.table
        className: 'table table-bordered'
        React.DOM.thead null,
          React.DOM.tr null,
            React.DOM.th null, 'Name'
            React.DOM.th null, 'Genres'
            React.DOM.th null, 'Actions'
        React.DOM.tbody null,
          for musician in @state.musicians
            React.createElement Musician, key: musician.id, musician: musician, genres: @props.genres, handleDeleteMusician: @deleteMusician, handleEditMusician: @updateMusician
