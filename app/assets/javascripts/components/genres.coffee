@Genres = React.createClass
  getInitialState: ->
    genres: @props.genres
  getDefaultProps: ->
    genres: []
  render: ->
    React.DOM.div
      className: 'genres'
      React.DOM.h2
        className: 'title'
        'Genres'
      React.DOM.div
        className: 'sending-mess'
        'sending..'
      React.createElement GenreForm, handleNewGenre: @props.addGenre
      React.DOM.hr null
      React.DOM.table
        className: 'table table-bordered'
        React.DOM.thead null,
          React.DOM.tr null,
            React.DOM.th null, 'Title'
            React.DOM.th null, 'Actions'
        React.DOM.tbody null,
          for genre in @props.genres
            React.createElement Genre, key: genre.id, genre: genre, handleDeleteGenre: @props.deleteGenre, handleEditGenre: @props.updateGenre
