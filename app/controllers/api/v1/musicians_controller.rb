class Api::V1:: MusiciansController < ApplicationController
before_action :set_musician, only: [:update, :destroy]

def index
  render json: Musician.all
end

# POST /musicians
# POST /musicians.json
def create
  @musician = Musician.new(musician_params)
  respond_to do |format|
    if @musician.save
      format.json { render json: @musician.to_json(:include => :genres) }
    else
      format.json { render json: @musician.errors, status: :unprocessable_entity }
    end
  end
end

# PATCH/PUT /musicians/1
# PATCH/PUT /musicians/1.json
def update
  respond_to do |format|
    # if @musician.update_with_custom_params(musician_params)
    if @musician.update(musician_params)
      format.json { render json: @musician.to_json(:include => :genres) }
    else
      format.json { render json: @musician.errors, status: :unprocessable_entity }
    end
  end
end

# DELETE /musicians/1
# DELETE /musicians/1.json
def destroy
  @musician.destroy
  respond_to do |format|
    format.json { head :no_content }
  end
end

private
# Use callbacks to share common setup or constraints between actions.
def set_musician
  @musician = Musician.find(params[:id])
end

# Never trust parameters from the scary internet, only allow the white list through.
def musician_params
  params.require(:musician).permit(:name, :genre_ids => [])
end
end
