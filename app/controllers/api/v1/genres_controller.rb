class Api::V1::GenresController < ApplicationController
  before_action :set_genre, only: [:update, :destroy]

  def index
    render json: Genre.all
  end

  # POST /genres
  # POST /genres.json
  def create
    @genre = Genre.new(genre_params)
    respond_to do |format|
      if @genre.save
        format.json { render json: @genre }
      else
        format.json { render json: @genre.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /genres/1
  # PATCH/PUT /genres/1.json
  def update
    respond_to do |format|
      if @genre.update(genre_params)
        format.json { render json: @genre }
      else
        format.json { render json: @genre.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /genres/1
  # DELETE /genres/1.json
  def destroy
    @genre.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_genre
    @genre = Genre.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def genre_params
    params.require(:genre).permit(:title)
  end
end
