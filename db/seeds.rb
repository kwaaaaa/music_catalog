# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if Genre.count == 0
  Genre.create( title: 'dub')
  Genre.create( title: 'alternative')
  Genre.create( title: 'reggae')
end


if Musician.count == 0
  3.times do
    m = Musician.create( name: "Chicago road ##{rand(100)}")
    m.genres << Genre.first
    m.genres << Genre.second
    m.save
  end
end
