class CreateGenresMusicians < ActiveRecord::Migration
  def change
    create_table :genres_musicians, id: false do |t|
      t.references :musician
      t.references :genre
    end
    add_index :genres_musicians, :musician_id
    add_index :genres_musicians, :genre_id
  end
end