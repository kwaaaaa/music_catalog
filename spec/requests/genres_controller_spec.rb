require 'rails_helper'

describe Api::V1::GenresController, type: :request do
  it 'sends a list of genres' do
    FactoryGirl.create_list(:genre, 10)

    get '/api/v1/genres'

    # test for the 200 status-code
    expect(response).to be_success

    # check to make sure the right amount of messages are returned
    expect(json.length).to eq(10)

    # check that the genre attributes are the same.
    expect(json.first['title']).to eq('genre title')
  end
end
