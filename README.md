[![Build Status](https://travis-ci.org/kwaaaaa/music_catalog.svg?branch=master)](https://travis-ci.org/kwaaaaa/music_catalog)

## Web-приложение "Cправочник музыкальных исполнителей".

Две визуальные таблицы: музыкантов и жанров. CRUD. Бэкэнд на RoR, фронтэнд на Bootstrap и React.